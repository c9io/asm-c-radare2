#include <stdio.h>

#define MAXLINE 1000

int getstr(char str[], int maxline);
void copy(char to[], char from[]);

int main() {

    
    char str[MAXLINE];
    int len;

   while( (len = getstr(str, MAXLINE)) > 0 || str[0] == '\0') {

       if(str[0] != '\0') {
            printf("%s", str);
       }
   }
}
//return length of str and array
int getstr(char str[], int max) {
    
    int i;
    int c;
    i = 0;

    while(i < max - 2 && (c = getchar()) != EOF && c != '\n') {

        str[i] = c;
        i++;
    }
    if (c == '\n') {
        while(str[i - 1] == '\t' || str[i - 1] == ' ') {
            i--;
        }
        if(i != 0) {
            str[i] = '\n';
            i++;
        }
        str[i] = '\0';
    }
    return i;
}

