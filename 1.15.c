#include <stdio.h>

float getfahr(float cels);

int main() {

    int min;
    int max;
    int step;    
    float fhar;
    float cels;

    step = 30;
    max = 300;
    min = 0;

    printf("---------------\n");
    printf("|%3s\t%6s|\n", "C", "F");
    printf("---------------\n");

    while (cels <= max) {
        fhar = getfahr(cels);
        printf("%3.0f\t%6.1f\n", cels, fhar);
        cels = cels + step;

    }
}
float getfahr(float cels) {

    return cels / (5.0/9.0) + 32.0;
    
}

