#include <stdio.h>

int main() {
    int ndig[10];
    int i;
    int c;
    int j;

    for (i = 0; i < 10; i++) {
        ndig[i] = 0;
    }
    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9') {
            
            ndig[c-'0']++;
        }
    }
    for (i = 0; i < 10; i++) {

        printf("%d ", i);
        for (j = 0; j < ndig[i]; j++) {
            printf("%s", "|");
        }
        printf("\n");
        
    }

}
