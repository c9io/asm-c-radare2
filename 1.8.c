#include <stdio.h>


int main() {

    int c;
    int space;
    int endstr;
    int tab;

    space = 0;
    endstr = 0;
    tab = 0;
    
    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            ++space;
        }else if (c == '\t') {
            ++tab;
        }else if (c == '\n') {
            ++endstr;
        }

    }
    printf("%d %d %d\n", space, tab, endstr);
}
