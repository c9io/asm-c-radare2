#include <stdio.h>

#define MAXLINE 1000

int getstr(char str[], int maxline);
void reverse(char to[], char from[], int len);

int main() {

    
    char str[MAXLINE];
    char revstr[MAXLINE];
    int len;

   while( (len = getstr(str, MAXLINE)) > 0) {

       reverse(revstr, str, len);
       
       printf("%s", revstr);

   }
}
//return length of str and array
int getstr(char str[], int max) {
    
    int i;
    int c;
    i = 0;

    while(i < max - 2 && (c = getchar()) != EOF && c != '\n') {

        str[i] = c;
        i++;
    }
    if (c == '\n') {
        while(str[i - 1] == '\t' || str[i - 1] == ' ') {
            i--;
        }
        if(i != 0) {
            str[i] = '\n';
            i++;
        }
        str[i] = '\0';
    }
    return i;
}

void reverse(char to[], char from[], int len) {
    int i;
    i = 0;
    while(len >= 0) {
        if (from[len] != '\n' && from[len] != '\0') {
            to[i] = from[len];   
            i++;
        }
        len--;
        
    }
    to[i] = '\n';
    i++;
    to[i] = '\0';



}





