#include <stdio.h>


int main() {

    int min;
    int max;
    int step;    
    float fhar;
    float cels;

    step = 30;
    max = 300;
    min = 0;

    printf("---------------\n");
    printf("|%3s\t%6s|\n", "C", "F");
    printf("---------------\n");

    while (cels <= max) {
        //cels = (5.0/9.0) * (fhar - 32.0);
        fhar = cels / (5.0/9.0) + 32.0;
        printf("%3.0f\t%6.1f\n", cels, fhar);
        cels = cels + step;

    }
}
