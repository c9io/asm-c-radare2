#include <stdio.h>

int main() {
    int ndig[26];
    int i;
    int c;
    int j;
    int max;
    max = 0;

    for (i = 0; i < 26; i++) {
        ndig[i] = 0;
    }
    while ((c = getchar()) != EOF) {
        if (c >= 'a' && c <= 'z') {
            
            ndig[c-'a']++;
        }
    }
    for (i = 0; i < 26; i++) {
        if (max < ndig[i]) {
            max = ndig[i];
        }
    }

    while (max > 0) {
        for (j = 0; j < 26; j++) {
            if (ndig[j] == max) {
                printf("|#|");
                ndig[j]--;
            } else {
                printf("| |");
            }
        }
        printf("\n");
        max--;
        
        
    }
    for (i = 0; i < 26; i++) {
            printf("|%c|", i + 'a');
        }
    printf("\n");


}
